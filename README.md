# CS371p: Object-Oriented Programming Allocator Repo

* Name: Simon Pinochet Concha

* EID: sp44454

* GitLab ID: simon.pinochet

* HackerRank ID: simon_pinochet

* Git SHA: c1b5be670038eed75a70d326a5c71d22188473ae

* GitLab Pipelines: https://gitlab.com/simon.pinochet/cs371p-allocator/pipelines

* Estimated completion time: 10 hours

* Actual completion time: 15 hours

* Comments:
