// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.h"

// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------

template <typename A>
struct TestAllocator2 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator<int,    100>,
             my_allocator<double, 100>>
             my_types_2;

TYPED_TEST_CASE(TestAllocator2, my_types_2);

TYPED_TEST(TestAllocator2, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator2, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(TestAllocator2, test_3) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type  s = 8;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(TestAllocator2, test_4) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    const allocator_type x;
    const size_type  s = 8;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// TEST 1: Allocate 1 value with just enough space
TEST(MyAllocator, my_test1) {
    my_allocator<int, 16> alloc = my_allocator<int, 16>();
    int* v = alloc.allocate(1);
    *v = 16;
    ASSERT_EQ(*v, 16);
}

// TEST 2: Allocate 1 value with a little more space than neede
TEST(MyAllocator, my_test2) {
    my_allocator<int, 20> alloc = my_allocator<int, 20>();
    int* v = alloc.allocate(1);
    *v = 20;
    ASSERT_EQ(*v, 20);
}

// TEST 3: Allocate 1 value with a lot more space than neede
TEST(MyAllocator, my_test3) {
    my_allocator<int, 100> alloc = my_allocator<int, 100>();
    int* v = alloc.allocate(1);
    *v = 100;
    ASSERT_EQ(*v, 100);
}

// TEST 4: Allocate multiple values with just enough space
TEST(MyAllocator, my_test4) {
    my_allocator<int, 88> alloc = my_allocator<int, 88>();
    int* v = alloc.allocate(10);
    for (int i = 0; i < 10; i++) {
        v[i] = i;
    }

    ASSERT_EQ(v[0], 0);
    ASSERT_EQ(v[1], 1);
    ASSERT_EQ(v[2], 2);
    ASSERT_EQ(v[3], 3);
    ASSERT_EQ(v[4], 4);
    ASSERT_EQ(v[5], 5);
    ASSERT_EQ(v[6], 6);
    ASSERT_EQ(v[7], 7);
    ASSERT_EQ(v[8], 8);
    ASSERT_EQ(v[9], 9);
}

// TEST 5: Deallocate block between busy blocks
TEST(MyAllocator, my_test5) {
    my_allocator<int, 48> alloc = my_allocator<int, 48>();
    int* v_1 = alloc.allocate(1);
    int* v_2 = alloc.allocate(1);
    int* v_3 = alloc.allocate(1);

    ASSERT_EQ(*(v_2 - 1), -8);
    alloc.deallocate(v_2, 8);
    ASSERT_EQ(*(v_2 - 1), 8);
}

// TEST 6: Deallocate block with free block to the left
TEST(MyAllocator, my_test6) {
    my_allocator<int, 48> alloc = my_allocator<int, 48>();
    int* v_1 = alloc.allocate(1);
    int* v_2 = alloc.allocate(1);
    int* v_3 = alloc.allocate(1);

    alloc.deallocate(v_1, 8);
    ASSERT_EQ(*(v_2 - 1), -8);
    alloc.deallocate(v_2, 8);
    ASSERT_EQ(*(v_1 - 1), 24);
}


// TEST 7: Deallocate block at the first positon
TEST(MyAllocator, my_test7) {
    my_allocator<int, 48> alloc = my_allocator<int, 48>();
    int* v_1 = alloc.allocate(1);
    int* v_2 = alloc.allocate(1);
    int* v_3 = alloc.allocate(1);

    ASSERT_EQ(*(v_1 - 1), -8);
    alloc.deallocate(v_1, 8);
    ASSERT_EQ(*(v_1 - 1), 8);
}

// TEST 8: Deallocate block with free block to the right
TEST(MyAllocator, my_test8) {
    my_allocator<int, 48> alloc = my_allocator<int, 48>();
    int* v_1 = alloc.allocate(1);
    int* v_2 = alloc.allocate(1);
    int* v_3 = alloc.allocate(1);

    alloc.deallocate(v_3, 8);
    ASSERT_EQ(*(v_2 - 1), -8);
    alloc.deallocate(v_2, 8);
    ASSERT_EQ(*(v_2 - 1), 24);
}


// TEST 9: Deallocate block at the last positon
TEST(MyAllocator, my_test9) {
    my_allocator<int, 48> alloc = my_allocator<int, 48>();
    int* v_1 = alloc.allocate(1);
    int* v_2 = alloc.allocate(1);
    int* v_3 = alloc.allocate(1);

    ASSERT_EQ(*(v_3 - 1), -8);
    alloc.deallocate(v_3, 8);
    ASSERT_EQ(*(v_3 - 1), 8);
}

// TEST 10: Deallocate block between free blocks
TEST(MyAllocator, my_test10) {
    my_allocator<int, 48> alloc = my_allocator<int, 48>();
    int* v_1 = alloc.allocate(1);
    int* v_2 = alloc.allocate(1);
    int* v_3 = alloc.allocate(1);

    alloc.deallocate(v_1, 8);
    alloc.deallocate(v_3, 8);

    ASSERT_EQ(*(v_2 - 1), -8);
    alloc.deallocate(v_2, 8);
    ASSERT_EQ(*(v_1 - 1), 40);
}

// TEST 11: Iterate through allocator
TEST(MyAllocator, my_test11) {
    my_allocator<int, 48> alloc = my_allocator<int, 48>();
    int* v_1 = alloc.allocate(1);
    int* v_2 = alloc.allocate(1);
    int* v_3 = alloc.allocate(1);

    auto b = alloc.begin();
    auto e = alloc.end();

    while (b != e) {
        ASSERT_EQ(*b, -8);
        b++;
    }
}

// TEST 12: Iterate through const allocator
TEST(MyAllocator, my_test12) {
    const my_allocator<int, 48> alloc = my_allocator<int, 48>();
    int* v_1 = alloc.allocate(1);
    int* v_2 = alloc.allocate(1);
    int* v_3 = alloc.allocate(1);

    auto b = alloc.begin();
    auto e = alloc.end();

    while (b != e) {
        ASSERT_EQ(*b, -8);
        b++;
    }
}

// TEST 13: Iterate through allocator back to front
TEST(MyAllocator, my_test13) {
    my_allocator<int, 48> alloc = my_allocator<int, 48>();
    int* v_1 = alloc.allocate(1);
    int* v_2 = alloc.allocate(1);
    int* v_3 = alloc.allocate(1);

    auto b = alloc.begin();
    auto e = alloc.end();

    e--;
    while (b != e) {
        ASSERT_EQ(*e, -8);
        e--;
    }
}

// TEST 14: Iterate through const allocator back to front
TEST(MyAllocator, my_test14) {
    const my_allocator<int, 48> alloc = my_allocator<int, 48>();
    int* v_1 = alloc.allocate(1);
    int* v_2 = alloc.allocate(1);
    int* v_3 = alloc.allocate(1);

    auto b = alloc.begin();
    auto e = alloc.end();

    e--;
    while (b != e) {
        ASSERT_EQ(*e, -8);
        e--;
    }
}

// TEST 15: Indexing
TEST(MyAllocator, my_test15) {
    // Normal
    my_allocator<int, 48> alloc = my_allocator<int, 48>();
    ASSERT_EQ(alloc[0], 40);
    ASSERT_EQ(alloc[44], 40);

    // Const
    const my_allocator<int, 48> _alloc = my_allocator<int, 48>();
    ASSERT_EQ(_alloc[0], 40);
    ASSERT_EQ(_alloc[44], 40);
}
