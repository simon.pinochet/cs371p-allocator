// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <iostream>  // cin, cout

const int OBJ_SIZE = 8;

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator& lhs, const my_allocator& rhs) {
        return lhs == rhs;
    }

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N + sizeof(int)];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const {
        const_iterator b = this->begin();
        const_iterator e = this->end();
        while (b != e) {
            ++b;
        }
        return true;
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return &(*lhs) == &(*rhs);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            char* temp = (char*) _p;
            int v = *_p;
            v = v > 0 ? v : v * -1;

            temp += 2 * sizeof(int) + v;
            _p = (int*) temp;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            char* temp = (char*) _p;
            int v = *(_p - 1);
            v = v > 0 ? v : v * -1;

            temp -= 2 * sizeof(int) + v;
            _p = (int*) temp;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = --*this;
            return x;
        }
    };

    // --------------
    // const_iterator
    // --------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return &(*lhs) == &(*rhs);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            char* temp = (char*) _p;
            int v = *_p;
            v = v > 0 ? v : v * -1;

            temp += 2 * sizeof(int) + v;
            _p = (int*) temp;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            char* temp = (char*) _p;
            int v = *(_p - 1);
            v = v > 0 ? v : v * -1;

            temp -= 2 * sizeof(int) + v;
            _p = (int*) temp;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        assert (N >= sizeof(T) + (2 * sizeof(int)));

        *(reinterpret_cast<int*>(&a[0])) = N - (2 * sizeof(int));
        *(reinterpret_cast<int*>(&a[N - sizeof(int)])) = N - (2* sizeof(int));
        *(reinterpret_cast<int*>(&a[N])) = 0;
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) const {
        if (n <= 0) {
            std::bad_alloc exception;
            throw exception;
        }

        int _n = n * OBJ_SIZE;
        const_iterator b = this->begin();
        const_iterator e = this->end();

        while (b != e) {
            if (*b < 0) {
                // Nothing
            }

            // n fits perfectly in b
            else if (*b == _n || ( *b > _n && (*b - _n) <= OBJ_SIZE )) {
                // Get sentinels
                char* sen_1 = (char*) &(*b);
                char* sen_2 = sen_1 + sizeof(int) + *b;

                // Set sentinels
                *(reinterpret_cast<int*>(sen_1)) *= -1;
                *(reinterpret_cast<int*>(sen_2)) *= -1;

                // Return first sentinel
                assert(valid());
                return (pointer) ((char*) sen_1 + sizeof(int));
            }

            // n fits in b
            else if (*b > _n) {
                // Get sentinels
                char* sen_1 = (char*) &(*b);
                char* sen_2 = sen_1 + sizeof(int) + _n;
                char* sen_3 = sen_2 + sizeof(int);
                char* sen_4 = sen_1 + sizeof(int) + *b;

                // Set sentinels
                *(reinterpret_cast<int*>(sen_3))  = *b - _n - sizeof(int) * 2;
                *(reinterpret_cast<int*>(sen_4))  = *b - _n - sizeof(int) * 2;
                *(reinterpret_cast<int*>(sen_1))  = -1 * _n;
                *(reinterpret_cast<int*>(sen_2))  = -1 * _n;

                // Return first sentinel
                assert(valid());
                return (const pointer) ((char*) sen_1 + sizeof(int));
            }

            //n didn't fit in b
            ++b;
        }

        // No available blocks
        assert(valid());
        return nullptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) const  {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type n) const {
        // Get sentinels
        char* sen_1 = ((char*) p) - sizeof(int);
        char* sen_2 = sen_1 + sizeof(int) - *(reinterpret_cast<int*>(sen_1));

        assert(*(reinterpret_cast<int*>(sen_1)) == *(reinterpret_cast<int*>(sen_2)));
        assert(n > 0);

        // Set sentinels
        *(reinterpret_cast<int*>(sen_1)) *= -1;
        *(reinterpret_cast<int*>(sen_2)) *= -1;

        // Check backwards
        if (sen_1 != a) {
            iterator b = iterator((int*) sen_1);
            --b;

            if (*b > 0) {
                *(reinterpret_cast<int*>(&(*b))) += 2 * sizeof(int) + *(reinterpret_cast<int*>(sen_1));
                *(reinterpret_cast<int*>(sen_2)) = *(reinterpret_cast<int*>(&(*b)));
                sen_1 = (char*) &(*b);
            }
        }

        // Check forwards
        if (sen_2 != &a[N - 4]) {
            iterator f = iterator((int*) sen_1);
            ++f;

            if (*f > 0) {
                *(reinterpret_cast<int*>(sen_1)) += 2 * sizeof(int) + *(reinterpret_cast<int*>(&(*f)));
                *(reinterpret_cast<int*>(((char*) &(*f)) + *f + 4)) = *(reinterpret_cast<int*>(sen_1));
            }
        }

        // Check if deallocate broke anything
        assert(valid());
    }

    // -----
    // print
    // -----

    /**
     * O(n) in spance
     * O(n) in time
     * print all data blocks
     */
#include <iostream> // cin, cout
    void print(std::ostream& w) {
        iterator b = this->begin();
        iterator e = this->end();

        while (b != e) {
            w << *b;
            ++b;
            if (b != e) std::cout << " ";
        }
        w << "\n";
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) const {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(reinterpret_cast<int*>(&a[0]));
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(reinterpret_cast<const int*>(&a[0]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator((int*) (&a[N]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator((const int*) (&a[N]));
    }
};

void allocator_run (std::istream& r, std::ostream& w);

#endif // Allocator_h
