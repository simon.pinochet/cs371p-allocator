// -----------
// Allocator.c++
// -----------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream>
#include <vector>

using namespace std;

#include "Allocator.h"

// -------------
// allocator_run
// -------------

void allocator_run (istream& r, ostream& w) {
    std::vector<int*> v;
    // Create allocator
    my_allocator<int, 1000> alloc = my_allocator<int, 1000>();

    // Iterate through tests
    string s;
    while (!r.eof()) {
        getline(r, s);
        if(s.empty()) break;

        // Read test
        int n = std::stoi(s);

        // Allocate or free
        if (n >= 0) alloc.allocate(n);
        else {
            n *= -1;
            my_allocator<int, 1000>::iterator it = alloc.begin();

            while (n > 0) {
                if (*it < 0) n--;
                if (n == 0) break;
                ++it;
            }

            alloc.deallocate(&(*it) + 1, sizeof(int));
        }
    }
    alloc.print(w);
}