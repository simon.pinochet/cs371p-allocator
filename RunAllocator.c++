// --------------
// RunAllocator.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream>
#include <vector>

using namespace std;

#include "Allocator.h"

// ----
// main
// ----

int main () {
    // Get number of test cases
    int num_cases;
    cin >> num_cases;
    string s;
    getline(cin, s);
    getline(cin, s);

    // Process test cases
    while (num_cases--) {
        allocator_run(cin, cout);
    }
    return 0;
}
